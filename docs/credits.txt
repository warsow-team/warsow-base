Founder
===========
Fabrice Demurger aka Solomonk


Programming
===========
Victor Luchits aka Vic
Medar
German Garcia aka Jal
Vitaliy Kuzmin aka SiPlus
Alex Seilder aka Toukkapoukka
Alexander Sundstrom aka Crizis


Mapping
===========
Ben Thompson aka 36
Jal
Blx
Crizis
Kim Kastås aka Kimza
Blemish
Anthy
Grumx
RiMoL
50u15pec7a70r
SolidFake


Visual design
===========
Jal
Crizis
BLx
Boqu
Adsn
Nanabaky
36
Matteo Pignatelli aka Pigna
Stylemistake
HazelH
Anthy
RiMoL
ReFly
Cha0s
Daze
Froop
SolidFake
M0t0
Cervix
Guwashi
Andrei Stepanov aka Adem
Clownfart


Sound design
===========
Jazz
Dol-B
Pigfreezer
nip


Soundtrack
===========
Jihnsius (R.I.P.)


Announcements
===========
Alex Popa aka Jehar


Website
===========
Crizis
Vic


Production
===========
Vic
Crizis
SiPlus


Acknowledgements
===========
The exwsw texture set is based on the original work by EvilLair (http://evillair.net).
The cha0swsw texture set is based on the original work by ydnar.
"Vox feminae" sound pack by HellChick.
"Stormy days" skybox by hipshot.


Special thanks
===========
[BE]Sable
1oook
A_Spec
Acid
Akimine
Alba
AlexMax
Ardsept
Atog007
Basic
Blazeh
Bleader
Bob_le_pointu
Brt
Budi & Etienne
Ch4ndl3r
Cl00m
Corrupted
Danlin
Decap
Dendrofil
Deu
Dexter
DJWheat
Dmh
DonKing
Drexciyan
Duby
E-troubadour Marco
E3d
Ekse
Eml
Emp
Ertai
Etienne
ETR
FallenAngel
Falconer
Fnatic Alba
Fng
François Demurger
FX
Garcimoore
Gladiac
Goyan
Goochie
Gunnarbot
H1o
Hangy
Hardyrah
Hellchick
HellKeeper
Hero
Hettoo
Himself
Huksi`
Iioran
Impii
Impulz
Jeghegy
Jonathan_thegrape
Jt-brako
Jyaan
K0li0
K3rmit
Kalhartt
Kakoo
Kalorie
Kapiter
Kerpal
Killer07
KominaAa
KoFFie
Koochi
Kriuq
Kurim
Lardarse
Learn_more
Legend
Lichen
ink
Lokirulez
Marrk
Mdoo
Memento_mori
Mikejs
Mikey
Mokshu
Mote
Mystikal
Muff
Nasa
Naymlis
Nealz
Nekon
NiCO
Nip
Noopi
Norman
Oscar
OZH
Panz3rfaust
ParK
Pat Aftermoon
Pb
Pierre
Pille
Poulet
Quee
Primevil
Razkun
Redeyes
Robinmitra1
Room
Rpg
Sandx
Sansai
scranusesscrunya
Serrafin
Shaun
Skc
Skywalk.er
Sonskusa
Squintik
SNooP
Stopiccot
Superiormassacre
TheLawenforcer
TyounanMUTI
Thoniel
Torbenh
TraitPlat
UnRuhe
Uz
Viagra
Virus
Vkni
What!
WTFProof
Will
Xeno
Yogolo
Zebus
Zerox
Zevon
Zkyp
Zyfo
