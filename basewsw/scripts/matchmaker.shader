gfx/ui/matchmaker/player
{
	nopicmip
	nomipmaps
	{
		map gfx/ui/matchmaker/ring.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
	}
	{
		map gfx/ui/matchmaker/tick.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
	}
}

gfx/ui/matchmaker/noplayer
{
	nopicmip
	nomipmaps
	{
		map gfx/ui/matchmaker/ring.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
	}
	{
		map gfx/ui/matchmaker/cross.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
	}
}

gfx/ui/matchmaker/ping
{
	nopicmip
	nomipmaps
	{
		map gfx/ui/matchmaker/ping.tga
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
	}
}
